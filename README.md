# sample-dai-solutions
This sample project is to show simple process on data retrieval from SF endpoint

## Getting started

Navigate to the project directory and get hands dirty.

### Create virtual environment

```bash
python -m venv venv
```

or

```bash
python -m pip install virtualenv
virtualenv venv
```

### Activate the virtual environment

```bash
source venv/Scripts/activate
```

### Install PostgreSQL

NA
<!-- Download [PostgreSQL](https://www.postgresql.org/download/). Tables will be created once you start the applications. We will use authors and books table to show gRPC usecase.
Create extension with default uuid-ossp module to generate universally unique identifiers (UUIDs).

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
``` -->

### Fast API server

#### Services

start any service in solutions_services folder with terminal or debug in vs code (launch.json)

##### sample with sobject service

```bash
cd solutions_services/sobject_service
python app/main.py
```

Endpoint specification will be available at [OpenApi](http://localhost:8002/api/v1/sobjects/docs) specification or alternative documentation with [ReDoc](http://127.0.0.1:8002/redoc)

### Deployment

#### Dockerfile

refer docker file in individual service package and run following command to build docker image (rancher need to be started beforehand)

```docker
docker build . -t sobject-service
```

#### kubernetes

Refer k8s folder to deploy the application From secret, deployment to service.

```bash
kubectl apply -f sobject-svc-deployment.yaml
```

some useful command

```bash
kubectl delete deployment/sobject-service

kubectl get all

kubeclt get svc

kubectl logs --tail 20  deployment/xxx

kubectl logs -l deploy=sobject --prefix -f

kubectl describe svc/sobject-service

kubectl describe pod/xxx

kubectl exec -it pod/xxx -- bash
```

## Support

Not all endpoint is error proofing for testing as this is POC application.Kindly modify the code to make it works.

This project can be renamed to DAI Solutions with frontend code included when we kickstart revamp initiative.

## Contributing

Thanks for everyone contribution. Comment and feedback are welcome.

## License

Copyright 2023 Eggplant
