from fastapi import APIRouter, HTTPException, Header, Path, Query, Request

import api.services as services
from api.constants import EMPTY_TOKEN_MESSAGE

validation_rule = APIRouter()


@validation_rule.get("", status_code=200)
async def get_validation_rules(
    request: Request,
    sobject_name: str | None = Query(alias="sobject-name"),
    metadata=False,
):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)
    response = await services.get_validation_rules(access_token, sobject_name, metadata)

    return {"data": response}


@validation_rule.get("/{record_type_id}", status_code=200)
async def get_validation_rule(request: Request, record_type_id: str):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)
    response = await services.get_validation_rule(access_token, record_type_id)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()[0]["message"]},
        )
    return {"data": response.json()}
