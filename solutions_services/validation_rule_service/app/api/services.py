import asyncio
from fastapi import HTTPException
import httpx

SALESFORCE_ENDPOINT = (
    "https://keysightsales--test1.sandbox.my.salesforce.com/services/data/v54.0"
)


async def get_validation_rules(access_token: str, sobject_name: str, metadata: bool):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/tooling/query?q=Select Id,Active from ValidationRule where EntityDefinition.QualifiedApiName = '{sobject_name}'",
            headers=headers,
        )

        if response.status_code != 200:
            raise HTTPException(
                status_code=response.status_code,
                detail={"title": "Error", "message": response.json()[0]["message"]},
            )

        records = response.json()["records"]
        validation_rule_list = []

        if metadata is False:
            return records
        else:
            tasks = []
            for record in records:
                url = f"{SALESFORCE_ENDPOINT}/tooling/sobjects/ValidationRule/{record['Id']}"
                tasks.append(asyncio.ensure_future(sf_client.get(url, headers=headers)))

            results = await asyncio.gather(*tasks)
            for result in results:
                validation_rule_list.append(result.json())

        return validation_rule_list


async def get_validation_rule(access_token: str, validation_rule_id: str):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/tooling/sobjects/ValidationRule/{validation_rule_id}",
            headers=headers,
        )
    return response
