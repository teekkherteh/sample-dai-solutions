from fastapi import FastAPI
import uvicorn
from api.validation_rule import validation_rule
from fastapi.middleware.gzip import GZipMiddleware

app = FastAPI(
    openapi_url="/api/v1/validation-rules/openapi.json",
    docs_url="/api/v1/validation-rules/docs",
)

app.add_middleware(GZipMiddleware, minimum_size=1000)

app.include_router(
    validation_rule, prefix="/api/v1/validation-rules", tags=["validation-rules"]
)
uvicorn.run(app, host="0.0.0.0", port=int(8003))
