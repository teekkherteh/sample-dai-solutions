from fastapi import FastAPI
import uvicorn
from api.auth import auth


app = FastAPI(openapi_url="/api/v1/auth/openapi.json", docs_url="/api/v1/auth/docs")

app.include_router(auth, prefix="/api/v1/auth", tags=["auth"])
uvicorn.run(app, host="0.0.0.0", port=int(8001))
