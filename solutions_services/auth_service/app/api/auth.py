from fastapi import APIRouter, HTTPException

from api.models import AuthModel
import api.services as services

auth = APIRouter()


@auth.post("/login", status_code=200)
async def get_sobject_layouts(payload: AuthModel):
    response = await services.sf_login(payload)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()["error_description"]},
        )
    return {"data": {"access_token": response.json()["access_token"]}}
