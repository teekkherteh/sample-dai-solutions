import httpx
from api.models import AuthModel
from urllib.parse import quote

SALESFORCE_URL = "https://keysightsales--test1.sandbox.my.salesforce.com/services"

access_token = ""


async def sf_login(payload: AuthModel):
    async with httpx.AsyncClient(http2=True) as sf_client:
        url = "/oauth2/token?grant_type=password"
        url += f"&client_id={payload.client_id}"
        url += f"&client_secret={payload.client_secret}"
        url += f"&username={payload.username}"
        url += f"&password={payload.password}"

        return await sf_client.post(f"{SALESFORCE_URL}" + quote(url, safe="/&=?"))
