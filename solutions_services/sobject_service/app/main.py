import os
from fastapi import FastAPI
import uvicorn
from api.sobject import sobject
from fastapi.middleware.gzip import GZipMiddleware

app = FastAPI(
    openapi_url="/api/v1/sobjects/openapi.json", docs_url="/api/v1/sobjects/docs"
)

app.add_middleware(GZipMiddleware, minimum_size=1000)

app.include_router(sobject, prefix="/api/v1/sobjects", tags=["sobjects"])
uvicorn.run(app, host="0.0.0.0", port=int(8002))
