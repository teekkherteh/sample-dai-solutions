import asyncio
import os
from fastapi import HTTPException
import httpx

# to be retrieve from DB
SALESFORCE_ENDPOINT = (
    "https://keysightsales--test1.sandbox.my.salesforce.com/services/data/v54.0"
)

print(os.environ.get("PGHOST", "localhost"))
print(os.environ.get("PGPORT", "5434"))
print(os.environ.get("PGUSER", "postgres"))
print(os.environ.get("PGPASSWORD", "admin"))


async def get_sobjects(access_token: str, creatable: bool):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/sobjects", headers=headers
        )

    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()[0]["message"]},
        )

    if creatable is True:
        return (
            sobject
            for sobject in response.json()["sobjects"]
            if sobject_predicate(sobject, creatable)
        )
    else:
        return response.json()["sobjects"]


async def get_sobject(access_token: str, sobject_name: str):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/sobjects/{sobject_name}/describe", headers=headers
        )
        # TBC - record insertion to sobject/ record type table
    return response


async def get_sobject_layout(access_token: str, sobject_name: str, record_type_id: str):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/sobjects/{sobject_name}/describe/layouts/{record_type_id}",
            headers=headers,
        )
        # TBC - record insertion to layout table
    return response


async def get_sobject_layouts(access_token: str, sobject_name: str):
    access_token = access_token
    async with httpx.AsyncClient(http2=True, timeout=20.0) as sf_client:
        headers = {"Authorization": access_token}  # Bearer ...
        sobject_response = await sf_client.get(
            f"{SALESFORCE_ENDPOINT}/sobjects/{sobject_name}/describe", headers=headers
        )
        sobject = sobject_response.json()
        # TBC - record insertion to sobject table

        # get record type layout
        tasks = []
        for record_type in sobject["recordTypeInfos"]:
            print(f'{sobject["name"]} - {record_type["recordTypeId"]}')
            url = f"{SALESFORCE_ENDPOINT}/sobjects/{sobject_name}/describe/layouts/{record_type['recordTypeId']}"
            tasks.append(asyncio.ensure_future(sf_client.get(url, headers=headers)))

        await asyncio.gather(*tasks)
        # TBC - record insertion to layout table
    return sobject_response  # not returning all layouts


def sobject_predicate(sobject, filter_flag: bool):
    if filter_flag:
        filter_key = [
            "keyPrefix",
            "createable",
            "deletable",
            "layoutable",
            "queryable",
            "retrieveable",
            "updateable",
            "undeletable",
            "searchable",
        ]
    else:
        filter_key = ["keyPrefix"]

    return all(
        sobject[key]
        # removed key
        # createable, deletable, layoutable, queryable, retrieveable,
        # updateable, undeletable, searchable
        for key in filter_key
    )
