from datetime import datetime
from fastapi import APIRouter, HTTPException, Path, Request

import api.services as services
from api.constants import EMPTY_TOKEN_MESSAGE

sobject = APIRouter()


@sobject.get("", status_code=200)
async def get_sobjects(request: Request, creatable: bool = True):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)

    response = await services.get_sobjects(access_token, creatable)

    return {"data": response}


@sobject.get("/{sobject_name}", status_code=200)
async def get_sobject(request: Request, sobject_name: str):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)
    response = await services.get_sobject(access_token, sobject_name)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()[0]["message"]},
        )
    return {"data": response.json()}


@sobject.get("/{sobject_name}/layouts", status_code=200)
async def get_sobject(request: Request, sobject_name: str):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)
    response = await services.get_sobject_layouts(access_token, sobject_name)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()[0]["message"]},
        )
    return {"data": response.json()}


@sobject.get("/{sobject_name}/layouts/{record_type_id}", status_code=200)
async def get_sobject_layouts(request: Request, sobject_name: str, record_type_id: str):
    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)
    response = await services.get_sobject_layout(
        access_token, sobject_name, record_type_id
    )
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail={"title": "Error", "message": response.json()[0]["message"]},
        )
    return {"data": response.json()}
