EMPTY_TOKEN_MESSAGE = {
    "title": "Could not authorize the request",
    "message": "Make sure the request has an Authorization header with a bearer token, of the form 'Authorization: Bearer [token]'",
}
