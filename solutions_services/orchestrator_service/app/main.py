from fastapi import FastAPI
import uvicorn
from api.orchestrator import orchestrator


app = FastAPI(
    openapi_url="/api/v1/orchestrator/openapi.json",
    docs_url="/api/v1/orchestrator/docs",
)

app.include_router(orchestrator, prefix="/api/v1/orchestrator", tags=["orchestrator"])
uvicorn.run(app, host="0.0.0.0", port=int(8088))
