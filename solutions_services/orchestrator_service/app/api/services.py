import asyncio
from fastapi import HTTPException
import httpx

SOBJECT_SERVICE_HOST_URL = "http://sobject-service:8002/api/v1/sobjects"
VALIDATION_RULE_SERVICE_HOST_URL = (
    "http://validation-rule-service:8003/api/v1/validation-rules"
)

# SOBJECT_SERVICE_HOST_URL = "http://localhost:30002/api/v1/sobjects"


async def update_sobject(access_token, count: int):
    async with httpx.AsyncClient(http2=True, timeout=20.0) as client:
        headers = {"Authorization": access_token}  # Bearer ...
        sobjects_response = await client.get(
            f"{SOBJECT_SERVICE_HOST_URL}", headers=headers
        )

        if sobjects_response.status_code != 200:
            raise HTTPException(
                status_code=sobjects_response.status_code,
                detail=sobjects_response.json()["detail"],
            )

        sobjects = sobjects_response.json()["data"]
        sample_sobjects = sobjects[0:count]  # choose first 5 to execute

        tasks = []
        for sobject in sample_sobjects:
            print(sobject["name"])

            # layouts
            url = f"{SOBJECT_SERVICE_HOST_URL}/{sobject['name']}/layouts"
            tasks.append(asyncio.ensure_future(client.get(url, headers=headers)))

            # validation rules
            url2 = f"{VALIDATION_RULE_SERVICE_HOST_URL}?sobject-name={sobject['name']}&metadata=true"
            tasks.append(asyncio.ensure_future(client.get(url2, headers=headers)))

        results = await asyncio.gather(*tasks)

        for result in results:
            print(result)
