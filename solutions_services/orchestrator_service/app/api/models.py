from pydantic import BaseModel


class AuthModel(BaseModel):
    client_id: str
    client_secret: str
    username: str
    password: str
