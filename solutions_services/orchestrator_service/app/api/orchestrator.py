from datetime import datetime
from fastapi import APIRouter, HTTPException, Request

from api.models import AuthModel
import api.services as services
from api.constants import EMPTY_TOKEN_MESSAGE

orchestrator = APIRouter()


@orchestrator.post("/sync-sobjects", status_code=200)
async def update_sobject(request: Request, count: int = 5):
    st = datetime.now()
    print("Start date and time is:", st)

    access_token = request.headers.get("Authorization")
    if access_token is None:
        raise HTTPException(status_code=401, detail=EMPTY_TOKEN_MESSAGE)

    await services.update_sobject(access_token, count)

    et = datetime.now()
    print("End date and time is:", et)
    print("Time taken in seconds:", datetime.timestamp(et) - datetime.timestamp(st))
