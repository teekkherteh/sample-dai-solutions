from datetime import datetime
import httpx
import asyncio


SOBJECT_SERVICE_HOST_URL = "http://localhost:30002/api/v1/sobjects"
access_token = "Bearer 00D8M0000000Mi8!ARoAQH0E_UOTQiBP8jtRxEmcgjR0DCIla0kyF.3uz8trKkRKAgjGK1syLNUSfCjTcXRx9hZ7cX6JGCZ5Nx_I.VAfVCVxdv8N"
client = httpx.AsyncClient(http2=True, timeout=20.0)


async def get_sobjects():
    headers = {"Authorization": access_token}  # Bearer ...
    response = await client.get(f"{SOBJECT_SERVICE_HOST_URL}", headers=headers)
    return response.json()["data"]


async def get_sobject(sobject_name: str):
    headers = {"Authorization": access_token}  # Bearer ...
    response = await client.get(
        f"{SOBJECT_SERVICE_HOST_URL}/{sobject_name}", headers=headers
    )
    return response.json()["data"]


async def get_sobject_layouts(sobject_name: str, record_type_id: str):
    headers = {"Authorization": access_token}  # Bearer ...
    response = await client.get(
        f"{SOBJECT_SERVICE_HOST_URL}/{sobject_name}/layouts/{record_type_id}",
        headers=headers,
    )
    print(response.json())
    return response.json()["data"]


async def main():
    st = datetime.now()
    print("Start date and time is:", st)

    sobjects = await get_sobjects()
    sample_sobjects = sobjects[0:5]  # choose first 5 to execute

    for sobject in sample_sobjects:
        print(sobject["name"])
        sobject = await get_sobject(sobject["name"])

        # get record type layout

        for record_type in sobject["recordTypeInfos"]:
            print(f'{sobject["name"]} - {record_type["recordTypeId"]}')
            record_type_layout = await get_sobject_layouts(
                sobject["name"], record_type["recordTypeId"]
            )
            print(
                f'{sobject["name"]} - {record_type["recordTypeId"]} - {record_type_layout["id"]}'
            )

    et = datetime.now()
    print("End date and time is:", et)
    print(
        "Total time taken in seconds:", datetime.timestamp(et) - datetime.timestamp(st)
    )


if __name__ == "__main__":
    asyncio.run(main())
